#include <string.h>

void sort_x86(char **noms, int n)
{
  char **ult = &noms[n - 1];
  char **p = noms;

  while (p < ult)
  {
    char *primer_apell = p[0];
    char *segundo_apell = p[1];

    while (*primer_apell != ' ')
      primer_apell++;

    while (*segundo_apell != ' ')
      segundo_apell++;

    if ((strcmp(primer_apell, segundo_apell) < 0) || (strcmp(primer_apell, segundo_apell) == 0 && strcmp(p[0], p[1]) <= 0))
      p++;
    else
    {
      char *tmp = p[0];
      p[0] = p[1];
      p[1] = tmp;
      p = noms;
    }
  }
}
